#!/usr/bin/python2.7

import sys 
import math
import argparse

# Tool to parse the output of the differential analisys,
# print it in human readable format, auomatically produce round
# compositions and find invariant properites

# Filter duplicate elements in a list
# from: https://www.peterbe.com/plog/uniqifiers-benchmark
# order preserving
def uniq_filter(seq, idfun=None): 
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result

def sort_uniq_filter(diffs, min_prob):
    diffs.sort(key = lambda (a, b, c, d, e): c, reverse=True)
    diffs = uniq_filter(diffs, lambda (a, b, c, d, e): (a, b, d, e) if c > min_prob else None );
    return diffs

# Parse the ouput of t310-diff
def parse_stat(content):
    zeroesin = []
    zeroes = []
    probs = []
    rounds = []
    keys = []
    for line in content:
        zeroesin.append(())
        zeroes.append(())
        curr = 0
        line_a = line.split(',')
        left= line_a[1].split(' ')[2].strip()
        for i in range(0, len(left)):
            if int(left[i]) != 0:
                zeroesin[-1] += (i+1,)

        right = line_a[2].strip().split(' ')
        for i in range(0, len(right)):
            if int(right[i]) != 0:
                zeroes[-1] += (i+1,)

        prob = float(line_a[0].split(' ')[1].strip())
        probs.append(prob)

        rn = int(line_a[3].strip())
        rounds.append(rn)

        if ( len(line_a) == 5 ):
            key = int(line_a[4].strip())
        else:
            key = 26
        keys.append(key)

    return zip(zeroesin, zeroes, probs, rounds, keys)

# Pretty-print differentials and probabilities
# @rounds: ignored
def pretty_print_stat(diffs, min_prob=0.0, teX=False):
    diffs = sort_uniq_filter(diffs, min_prob)
    diffs.sort(key = lambda (a, b, c, d, e): (-c, (a, b, d, e)), reverse=False)

    for (indiff, outdiff, prob, rn, k) in diffs:
        prob = math.log(prob, 2) 
        prob = math.ceil(prob*100)/100
        if teX:
            print str(k) + "& " + str(rn) + "& " + str(list(indiff)) + " $\\to$ " + str(list(outdiff)) + " & $2^{" + str(prob) +"} $\\\\"
        else:
            print "Differential: " + str(indiff) + " => " + str(outdiff) + ",\t" + \
                    "Prob: 2^" + str(prob) + ",\t" + "Rounds: " + str(rn) + ", Key: " + str(k)



# Try to find invariant properties
def find_invariant(diffs, prob):
    diffs = sort_uniq_filter(diffs, prob)
    (Din, Dout, probs, rounds, keys) = zip(*diffs)
    for head in Din:
        path = invariant_rec([head], Din, Dout)
        if path != None:
            print "Found!"
            print path
            return path
    print "No invariant found!"

def invariant_rec(path, Din, Dout):
    possible_steps = [ Dout[x] for x in range(0,len(Din)) if Din[x] == path[-1] ]
    #print path, "st", possible_steps  
    for step in possible_steps:
        if step == path[0]:
            path.append(step)
            return path 
        else:
            newpath = list(path)
            newpath.append(step)
            newpath = invariant_rec(newpath, Din, Dout)
            if newpath != None:
                return newpath

# Try to find new differentials by composition
def join_rounds(diffs, prob, rounds, teX=False):
    diffs = sort_uniq_filter(diffs, prob)
    res = []
    for (Din, Dout, p, r, k) in diffs:
        res += find_paths([Din], 1, rounds, diffs, prob, rounds) 

    #print res
    pretty_print_stat(res, prob, teX)
    return res

def find_paths(path, path_prob, path_rounds, diffs, prob, rounds):
    res = []
    possible_steps = [x for x in diffs if x[0] == path[-1]]
    #print "Path", path, "PS", possible_steps, "rn", path_rounds
    for (Din, Dout, p, r, k) in possible_steps:
        if r == path_rounds:
            #print "added", path, Dout
            res.append( (path[0], Dout, p*path_prob, rounds, k) )
        elif r < path_rounds:
            newpath = list(path)
            newpath.append(Dout);
            paths = find_paths(newpath, p*path_prob, path_rounds-r, diffs, prob, rounds) 
            res += paths 

    return res
    

# Main
if  __name__  == '__main__':
    ps = argparse.ArgumentParser(description='Process differential properties.')
    ps.add_argument('files', metavar='file', type=argparse.FileType('r'), nargs='+', help='file name(s)')
    ps.add_argument('--print', dest='prnt', action='store_true',
                    help='pretty print stats')
    ps.add_argument('-i', '--inv', action='store_true',
                    help='search for invariants')
    ps.add_argument('-j', '--join-rounds', dest='jr', action='store_true',
                    help='join the rounds ')
    ps.add_argument('-X', '--teX', dest='teX', action='store_true',
                    help='output teX format for differentials ')
    ps.add_argument('-p', '--min-prob', dest='prob', metavar='prob', type=float, default=0.0,
                    help='filter differential with probability lower than prob')
    ps.add_argument('-r', '--rounds', dest='rounds', metavar='rounds', type=int, 
            help='number of rounds')

    args = ps.parse_args()

    content = []
    for f in args.files:
        content_r = f.readlines()
        content += [x.strip() for x in content_r] 
    diffs = parse_stat(content)

    prnt = True
    if (args.inv or args.jr) and not args.prnt:
        prnt = False

    if prnt:
        pretty_print_stat(diffs, args.prob, args.teX)
    if args.jr:
        join_rounds(diffs, args.prob, args.rounds, teX=args.teX)
    if args.inv:
        find_invariant(diffs, args.prob);
