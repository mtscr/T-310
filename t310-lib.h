#ifndef _T310_CRYPTO_LIB
#define _T310_CRYPTO_LIB

#define INSIZE 37
#define OUTSIZE 37

void T310BlockPhiEncryptOneRound(
		int s1, int s2, int f,
		int output[OUTSIZE],
		int input[INSIZE]
		);

#endif
