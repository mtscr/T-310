#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <openssl/md5.h>
 
#include "t310-lib.h"
#include "t310-utils.h"

// Real INSIZE: we use one extra bit for halting
#define INSIZE_R INSIZE+1

// Input space: set to INSIZE-1 to test over all possible inputs
#define INSPACE INSIZE-1

//#define PRINT
#define PART_STEP 25

void incremental_mode(const struct cipher_conf * cconf, 
		void (*increment)(int *, int *, const int),
		int * start, int * set, 
		uint64_t stop, 
		const struct diff_conf * dconf);

void hamming_stat_mode(const struct cipher_conf * cconf, 
		int * start, int * set, uint64_t stop, 
		int deltain,
		const struct diff_conf * dconf);

void diff(const struct cipher_conf * cconf, const int * bitmask, 
		const struct diff_conf * dconf,
		double * final_perc );

inline int differential(const int * in, const int * out, 
		int * bitmask, int size);

// Configuration for one round
struct cipher_conf {
	int s1, s2, 				// Key bits
		f;						// Iv bit
};

// Internal configuration
struct diff_conf {
	uint32_t rounds;			// Number of rounds of phi to run
	uint64_t threshold;			// Minimum random samples to collect 
	double min_perc,			// Differential less probable than this will be ignored
		   gain,				// How much the error in a sample affect variance 
		   max_variance;		// Variance to reach before stoppin sampling
	bool invert;				// Look for delta-out with hw > 32 instead of <3
};

// Internal ouput format
struct diff_res {
	double final_perc;			// 
	int * flip_index;			// Array containg delta-out*samples
	int * diff_distribution;	// Array containg distribution of hamming weights 
};

int main(int argc, char ** argv) {
	int opt;
	enum modes { DIFF, HAMMING, UNSET } mode;
	bool exponential = false, 
		 permutation = false,
		 bitsetm = false,
		 startstr = false,
		 hamming = false;

	struct cipher_conf cconf;
	struct diff_conf dconf;
	void (*step_fun)(int * in, int * set, const int size);

	int bitmask[INSIZE_R] = {0},	// delta-in
		bitset[INSIZE_R] = {0};		// subset of delta-in bits to analyze
	char bitset_str[INSIZE_R] = {0};

	int rounds = 3,
		deltain = 0;
	uint8_t key = 7;
	uint64_t start = 0,
			 end = -1,
			 thresh = 1000;
	double min_perc = 0.1,
		   variance = 0.01;
	bool invert = false;

	step_fun = increment_unit;
	one_fill(bitset, INSIZE+1);
	mode = UNSET;
	
	while ( (opt = getopt(argc, argv, "r:p:s:e:k:iXB:S:v:t:H:P:")) != -1 ) {
		switch (opt) {
			case 'r':
				rounds = strtol(optarg, NULL, 10);
				assert( rounds > 0 );
				break;
			case 'p':
				min_perc = strtod(optarg, NULL);
				break;
			case 's':
				start = strtol(optarg, NULL, 10);
				assert( start >= 0 );
				break;
			case 'S':
				startstr = true;
				if ( permutation ) {
					goto invalid_cmd;
				}
				assert( strlen(optarg) == INSIZE-1 );
				string_fill(bitmask, optarg, INSIZE);
				break;
			case 'e':
				end = strtol(optarg, NULL, 10);
				assert( end > 0 );
				break;
			case 'k':
				key = strtol(optarg, NULL, 10);
				assert( key >= 0 && key <= 7 );
				break;
			case 'i':
				invert = true;
				break;
			case 'X':
				exponential = true;
				if ( hamming || permutation ) {
					goto invalid_cmd;
				}
				mode = DIFF;
				step_fun = increment_exp;
				break;
			case 'B':
				bitsetm = true;
				if ( hamming || permutation ) {
					goto invalid_cmd;
				}
				assert( strlen(optarg) == INSIZE-1 );
				strncpy(bitset_str+1, optarg, INSIZE-1);
				string_fill(bitset, bitset_str, INSIZE);
				bitset[INSIZE] = 1;
				if ( exponential ) {
					step_fun = increment_exp_s;
				} else {
					step_fun = increment_unit_s;
				}
				break;
			case 'v':
				variance = strtod(optarg, NULL);
				assert( variance >= 0 );
				break;
			case 't':
				thresh = strtol(optarg, NULL, 10);
				assert( thresh >= 1000 );
				break;
			case 'H':
				hamming = true;
				if ( exponential || bitsetm || permutation ) {
					goto invalid_cmd;
				}
				// Compute hamming weights
				mode = HAMMING;
				deltain = strtol(optarg, NULL, 10);
				assert( deltain > 0 && deltain < INSIZE );
				// In hamming mode, min_perc is used as the minimum hamming weight to consider	
				min_perc = 32;
				break;
			case 'P':
				permutation = true;
				if ( exponential || bitsetm || hamming || startstr ) {
					goto invalid_cmd;
				}
				mode = DIFF;
				fprintf(stderr, 
						"WARING: permutation mode is experimental "
						"AND does not halt automatically (use -e) \n" );

				deltain = strtol(optarg, NULL, 10);
				assert( deltain > 0 && deltain < INSIZE );
				step_fun = increment_perm;
				element_0(bitmask, deltain, INSIZE);
				break;
			default:
				return -1;
				break;
		}
    }
	// Default mode: differential
	if ( mode == UNSET ) {
		mode = DIFF;
	}

	// init RNG
	srand(time(NULL));

	// Set all key/IV bits at the lower 3 bits of paramenter key
	cconf.s1 = key &1;
	cconf.s2 = (key >> 1) &1;
	cconf.f = (key >> 2)&1;

	dconf.gain = 0.2;
	// Threshold needs to be >10. A much bigger value (>1000) is needed 
	// to archieve significant results.
	dconf.threshold = thresh;
	dconf.max_variance = variance;
	dconf.rounds = rounds; 
	dconf.min_perc = min_perc;
	dconf.invert = invert;

	for (uint64_t i = 0; i < start; i++ ) {
		step_fun(bitmask, bitset, INSIZE);
	}

	if ( mode == DIFF ) {
		incremental_mode(&cconf, step_fun, bitmask, bitset, end, &dconf);
	} else if ( mode == HAMMING ) {
		hamming_stat_mode(&cconf, bitmask, bitset, end, deltain, &dconf);
	} 
	
	return 0;

invalid_cmd:
	printf("Invalid combination of options\n");
	return -1;
}

inline int differential(const int * in, const int * out, 
		int * bitmask, int size) {
	// hamming weight
	int diff = 0;
	for ( int j = 1; j < size; j++ ) { 
		if ( ( in[j] != out[j] ) ) {
			diff++;
			if ( bitmask ) {
				bitmask[j]++;
			}
		}
	}
	return diff;
}

void diff(const struct cipher_conf * cconf, const int * bitmask, 
		const struct diff_conf * dconf,
		struct diff_res * res ) {
	static int out1[2][INSIZE_R] = {{0}};
	static int out2[2][INSIZE_R] = {{0}};
	static int in[INSIZE_R] = {0};
	int * good_diff_idxs = res->flip_index;
	int s1 = cconf->s1,
		s2 = cconf->s2, 
		f = cconf->f;
	double gain = dconf->gain,
		   max_variance = dconf->max_variance;
	uint64_t thresh = dconf->threshold;
	uint32_t rounds = dconf->rounds;
	bool inv = dconf->invert;

	//  *   Approach 1: fixed bitmap, test n inputs
	int diff = 0, diff_idxs[INSIZE] = {0};
	uint64_t diffs = 0,
			 good_diffs = 0;
	double err = 0,
		   average = 0.5,
		   mdev = 1;

	zero_fill(diff_idxs, INSIZE);

	// We stop computing differental after thresh computation,
	// if the variance of "good differentials" (1 bit or 35 bits) 
	// is too low or too hight
	while ( diffs < thresh || ( mdev > max_variance && mdev < 0.7 ) ) {

		for ( uint64_t k = 0; k < thresh/10; k++ ) {
			random_fill_md5(in, INSIZE);
			zero_fill(diff_idxs, INSIZE);

			T310BlockPhiEncryptOneRound(s1, s2, f, out1[0], in); 
			for ( uint32_t i = 0; i < rounds-1; i++ ) {
				// Run ROUNDS round of phy on the input
				// NOTE: key bits and IV fixed!
				T310BlockPhiEncryptOneRound(s1, s2, f, out1[(i+1)%2], out1[i%2]); 
			}

			flip(in, bitmask, INSIZE);

			T310BlockPhiEncryptOneRound(s1, s2, f, out2[0], in); 
			for ( uint32_t i = 0; i < rounds-1; i++ ) {
				T310BlockPhiEncryptOneRound(s1, s2, f, out2[(i+1)%2], out2[i%2]); 
			}
			diff = differential(out1[(rounds-1)%2], out2[(rounds-1)%2], diff_idxs, INSIZE);

			diffs++;
			// We are only looking for 1-bit //or 35-bit differentials

			if ( (!inv && diff <= 2) || (inv && diff > 32) ) {
				good_diffs++;
				sum_vec(good_diff_idxs, diff_idxs, INSIZE);
			}
		}
		err = (double)good_diffs/(double)diffs - average;
		average = average + gain * err;
		if ( err < 0 )
			err = 0-err;
		mdev = mdev + 0.1*( err - mdev );
	}

	res->final_perc = (double)good_diffs/(double)diffs;
}

void incremental_mode(const struct cipher_conf * cconf, 
		void (*increment)(int *, int *, const int),
		int * start, int * set,
		uint64_t stop, 
		const struct diff_conf * dconf) {
	uint64_t count = 0;
	int * bitmask = start;
	int flips[INSIZE_R] = {0};
	struct diff_res dres;
	dres.flip_index = flips;

	increment(bitmask, set, INSIZE);
	while ( bitmask[INSPACE+1] < 1 && count < stop ) {
		zero_fill(flips, INSIZE);
		count++;

		diff(cconf, bitmask, dconf, &dres ); 

		if ( dres.final_perc > dconf->min_perc ) { 
			printf("%%: %f, ", dres.final_perc );
			printf("bitmask: ");
			print_vec(bitmask, INSIZE);
			printf(", ");
			print_vec_spaced(dres.flip_index, INSIZE);
			printf(", %d\n", dconf->rounds);
			fflush(stdout);
		}
		increment(bitmask, set, INSIZE);
	}
}

void diff_num(const struct cipher_conf * cconf, const int * bitmask, 
		const struct diff_conf * dconf,
		struct diff_res * res ) {
	static int out1[2][INSIZE_R] = {{0}};
	static int out2[2][INSIZE_R] = {{0}};
	static int in[INSIZE_R] = {0};
	int * diff_distribution = res->diff_distribution,
		* flip_index = res->flip_index;
	int s1 = cconf->s1,
		s2 = cconf->s2, 
		f = cconf->f;
	double gain = dconf->gain,
		   max_variance = dconf->max_variance;
	uint64_t thresh = dconf->threshold;
	uint32_t rounds = dconf->rounds;

	//  *   Approach 1: fixed bitmap, test n inputs
	int diff = 0, 
		diff_idxs[INSIZE] = {0},
		best_diff = (int) dconf->min_perc;
	uint64_t diffs = 0;
	double err = 0,
		   average = 0.5,
		   mdev = 1;

	zero_fill(diff_idxs, INSIZE);

	while ( diffs < thresh || ( mdev > max_variance && mdev < 0.7 ) ) {

		for ( uint64_t k = 0; k < thresh/10; k++ ) {
			random_fill_md5(in, INSIZE);
			zero_fill(diff_idxs, INSIZE);

			T310BlockPhiEncryptOneRound(s1, s2, f, out1[0], in); 
			for ( uint32_t i = 0; i < rounds-1; i++ ) {
				T310BlockPhiEncryptOneRound(s1, s2, f, out1[(i+1)%2], out1[i%2]); 
			}

			flip(in, bitmask, INSIZE);

			T310BlockPhiEncryptOneRound(s1, s2, f, out2[0], in); 
			for ( uint32_t i = 0; i < rounds-1; i++ ) {
				T310BlockPhiEncryptOneRound(s1, s2, f, out2[(i+1)%2], out2[i%2]); 
			}
			diff = differential(out1[(rounds-1)%2], out2[(rounds-1)%2], diff_idxs, INSIZE);

			diffs++;
			diff_distribution[diff]++;
			if ( diff > best_diff ) {
				copy_vec(flip_index, diff_idxs, INSIZE); 
				best_diff = diff;
			}
		}
		// We can estimate the error checking whether a diff distribution 
		// is increasing linearly.
		// Probably using mean of diff_distribution would be better
		//good_diffs = diff_distribution[32];
		err = diff_distribution[30]/(diff_distribution[29]+1) - average;
		average = average + gain * err;
		if ( err < 0 )
			err = 0-err;
		mdev = mdev + 0.1*( err - mdev );
	}
	res->final_perc = best_diff;
}

void hamming_stat_mode(const struct cipher_conf * cconf, 
		int * start, int * set, uint64_t stop, 
		int deltain,
		const struct diff_conf * dconf) {
	void (*increment)(int *, int *, const int);
	uint64_t count = 0, total = 0;
	int * bitmask = start;
	int diff_distribution[INSIZE_R] = {0},
		flips[INSIZE_R] = {0};
	struct diff_res dres;
	dres.flip_index = flips;
	dres.diff_distribution = diff_distribution;

	total = deltain > INSIZE -1 -deltain ?
		fact(INSIZE - 1, deltain) /  fact(INSIZE - 1 - deltain, 1)
		:
		fact(INSIZE - 1, (INSIZE - 1 - deltain)) /  fact(deltain, 1);

	element_0(bitmask, deltain, INSIZE);
	increment = increment_perm;

	count = 0;
	while ( (bitmask[INSPACE+1] < 1) && count < stop ) {
		zero_fill(flips, INSIZE);
		count++;

		diff_num(cconf, bitmask, dconf, &dres ); 

		if ( dres.final_perc > dconf->min_perc) {
			printf("flips: %d", deltain);
			printf(", ");
			print_vec(bitmask, INSIZE);
			printf(", ");
			print_vec_spaced(flips, INSIZE);
			printf(", %d\n", dconf->rounds);
		}

		if ( !( count % 2 ) )
				fprintf(stderr, "PROGRESS: %lu/%lu\n", count, total);
			
		increment(bitmask, set, INSIZE);
	}

	printf("deltain: %d", deltain);
	printf(", distrib_of_deltaout: ");
	print_vec_spaced(diff_distribution, INSIZE);
	printf(", rounds: %d\n", dconf->rounds);
	fflush(stdout);
}
