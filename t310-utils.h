#ifndef __T310_UTIL_LIB_
#define __T310_UTIL_LIB_ 

// Various increment modes for inputs/bitmasks
inline void increment_unit(int * in, int * set, const int insize);
inline void increment_unit_s(int * in, int * set, const int insize);
inline void increment_exp(int * in, int * set, const int insize);
inline void increment_exp_s(int * in, int * set, const int insize);
inline void increment_perm(int * in, int * t, const int insize);
inline void element_0(int * in, int ones, const int insize);

// Utils for arrays of real size (size+1) (last element used
// for halting computations)
inline void flip(int * in, const int * bitmask, const int size);
inline void random_fill(int * in, const int size);
inline void zero_fill(int * in, const int size);
inline void one_fill(int * in, const int size);
inline void string_fill(int * in, char * str, const int size);
inline void print_vec(int * in, const int size);


///////////////////////////////////
// inline functions defined HERE //
///////////////////////////////////
inline void increment_unit(int * in, int * set, const int insize) {
	in[1]++;
	for ( int j = 1; j < insize; j++ ) { 
		// insize = INSIZE_R - 1 : in[37] will be the last increment 
		// before the while loop stops
		if ( in[j] > 1 ) {
			in[j] = 0;
			in[j+1]++; 
		}
	}
}

inline void increment_unit_s(int * in, int * set, const int insize) {
	for ( int i = 1; i < insize; i++ ) {
		if ( set[i] ) {
			in[i]++;
			break;
		}
	}
	for ( int j = 1; j < insize; j++ ) { 
		if ( in[j] > 1 ) {
			in[j] = 0;
			for ( int i = j+1; i < insize+1; i++ ) {
				if ( set[i] ) {
					in[i]++; 
					break;
				}
			}
		}
	}
}

inline void increment_exp(int * in, int * set, const int insize) {
	for ( int j = 1; j < insize; j++ ) { 
		if ( in[j] == 1 ) {
			in[j] = 0;
			in[j+1]++; 
			return;
		}
	}
	in[1] = 1;
}

inline void increment_exp_s(int * in, int * set, const int insize) {
	for ( int j = 1; j < insize; j++ ) { 
		if ( in[j] == 1 ) {
			in[j] = 0;
			for ( int i = j+1; i < insize+1; i++ ) {
				if ( set[i] ) {
					in[i] = 1; 
					break;
				}
			}
			return;
		}
	}
	for ( int i = 1; i < insize; i++ ) {
		if ( set[i] ) {
			in[i] = 1;
			break;
		}
	}
}

inline void element_0(int * in, int ones, const int insize) {
	for ( int i = 1; i < ones + 1; i++ ) {
		in[i] = 1;
	}
	for ( int i = ones + 1; i < insize; i++ ) {
		in[i] = 0;
	}
}

inline int next_perm(uint64_t v) {
	//Taken from http://graphics.stanford.edu/~seander/bithacks.html
	//unsigned int v; // current permutation of bits 
	uint64_t w; // next permutation of bits

	uint64_t t = v | (v - 1); // t gets v's least significant 0 bits set to 1
	// Next set to 1 the most significant bit to change, 
	// set to 0 the least significant ones, and add the necessary 1 bits.
	// ctz: count consecutive trailing zeros
	w = (t + 1) | (((~t & -~t) - 1) >> (__builtin_ctz(v) + 1));  

	//t = (v | (v - 1)) + 1;  
	//w = t | ((((t & -t) / (v & -v)) >> 1) - 1); 
	return w;
}

inline void vec_next_perm(int * in, int * t, const int insize) {
	int ctz = 0,
		rightmost_zero = 0,
		ones = 0;
	// t = v | (v - 1 )
	for ( int i = 1; i < insize; i++ ) {
		if ( in[i] == 0 ) {
			t[i] = 1;
		} else {
			ctz = i - 1;
			break;
		}
	}
	for ( int i = ctz + 1; i < insize; i++ ) {
		t[i] = in[i];
	}

	// (~t & -~t)
	for ( int i = 1; i < insize; i++ ) {
		if ( t[i] == 0 ) {
			rightmost_zero = i;
			break;
			// (~t & -~t)-1 = 
			// (rightmost_zero - 1) trailing ones
		}
	}

	ones = rightmost_zero - 1 - (ctz+1);
	if ( ones < 0 ) {
		// halt
		in[insize] = 1;
		return;
	}

	increment_unit(t, NULL, insize);
	
	for ( int i = 1; i < ones + 1; i++ ) {
		in[i] = 1;
	}

	for ( int i = ones + 1; i < insize; i++ ) {
		in[i] = t[i];
	}
	
} 

inline void increment_perm(int * in, int * t, const int insize) {
	static bool halt = false;

	vec_next_perm(in, t, insize);

	halt = true;
	for ( int i = 1; i < insize; i++ ) {
		if ( in[i] == 1 ) {
			halt = false;
		}
	}

	if ( halt ) {
		in[insize] = 1;
	}
}

inline void flip(int * in, const int * bitmask, const int size) {
	// hamming weight
	for ( int j = 1; j < size; j++ ) { 
		in[j] ^= bitmask[j];
	}
}

inline void random_fill(int * in, const int size) {
	// Only good if size < 64
	// biased if size != 64
	uint64_t r = (((uint64_t) rand()) << 32) + (uint64_t) rand();
	for ( int j = 1; j < size; j++ ) { 
		in[j] = r & 1;
		r = r >> 1;
	}
}

inline void random_fill_md5(int * in, const int size) {
	static uint64_t seed = rand();
	uint64_t r;
	MD5((unsigned char *) &seed, 8, (unsigned char *) &r);
	seed = r;
	for ( int j = 1; j < size; j++ ) { 
		in[j] = r & 1;
		r = r >> 1;
	}
}

inline void zero_fill(int * in, const int size) {
	for ( int j = 1; j < size; j++ ) { 
		in[j] = 0;
	}
}

inline void one_fill(int * in, const int size) {
	for ( int j = 1; j < size; j++ ) { 
		in[j] = 1;
	}
}

inline void string_fill(int * in, char * str, const int size) {
	for ( int j = 1; j < size; j++ ) { 
		in[j] = str[j]&1;
	}
}

inline void sum_vec(int * out, int * in, const int size) {
	for ( int j = 1; j < size; j++ ) { 
		out[j] += in[j];
	}
}

inline void copy_vec(int * out, int * in, const int size) {
	for ( int j = 1; j < size; j++ ) { 
		out[j] = in[j];
	}
}

inline void print_vec(int * in, const int size) {
	for ( int j = 1; j < size; j++ ) {
		printf("%1d", in[j]);
	}
}

inline void print_vec_spaced(int * in, const int size) {
	for ( int j = 1; j < size-1; j++ ) {
		printf("%d ", in[j]);
	}
	printf("%d", in[size-1]);
}

inline uint64_t fact( uint64_t n, uint64_t t) {
	uint64_t res = 1;
	for ( uint64_t i = t+1; i <= n && res < (uint64_t) -1; i++ ) {
		res *= i;
	}
	return res;
}
#endif
