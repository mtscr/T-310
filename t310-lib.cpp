#include "t310-lib.h"
#include <stdio.h>

//9=>1
inline int T310ZFunction(int e1,int e2,int e3,int e4,int e5,int e6)
{
	int sum=
		e1 + e5 + e6
		+ e1*e4 + e2*e3 + e2*e5 + e4*e5 + e5*e6
		+ e1*e3*e4 + e1*e3*e6 + e1*e4*e5 + e2*e3*e6 + e2*e4*e6 + e3*e5*e6
		+ e1*e2*e3*e4 + e1*e2*e3*e5 + e1*e2*e5*e6 + e2*e3*e4*e6 + e1*e2*e3*e4*e5
		+ e1*e3*e4*e5*e6;
	return !(sum & 1);//mod 2
}

//29=>9
inline void T310TFunction(
		int &t1,int &t2,int &t3,int &t4,int &t5,int &t6,int &t7,int &t8,int &t9,
		int e00,int e01,int e02,int e03,int e04,int e05,int e06,int e07,int e08,int e09,
		int e10,int e11,int e12,int e13,int e14,int e15,int e16,int e17,int e18,int e19,
		int e20,int e21,int e22,int e23,int e24,int e25,int e26,int e27,int e28)
{
	t1 = e00;
	t2 = t1 + T310ZFunction(e01,e02,e03,e04,e05,e06); t2 &= 1; //mod2
	t3 = t2 + e07; t3 &= 1; //mod2
	t4 = t3 + T310ZFunction(e08,e09,e10,e11,e12,e13); t4 &= 1; //mod2
	t5 = t4 + e14; t5 &= 1; //mod2
	t6 = t5 + e01 + T310ZFunction(e15,e16,e17,e18,e19,e20); t6 &= 1; //mod2
	t7 = t6 + e21; t7 &= 1; //mod2
	t8 = t7 + T310ZFunction(e22,e23,e24,e25,e26,e27); t8 &= 1; //mod2
	t9 = t8 + e28; t9 &= 1; //mod2
}

//hard coded part of D, NOT the only possibility
// Here: key 26
int j[9] = {-1,3,7,2,6,5,8,4,9};

//hard coded part of P, NOT the only possibility
int p[28] = {
	-1,
	8, 4, 33, 16, 31, 20, 5,
	35, 9, 3, 19, 18, 12, 7,
	21, 13, 23, 25, 28, 36, 24,
	15, 26, 29, 27, 32, 11
};

int d[10] = {
	-1,
	0, 28, 4, 32, 24, 8, 12, 20, 16
};

//input x=1..9 output=0..36
int D(int x){ 
	return d[x];
}

//input x=1..27 output=1..36
int P(int x){ return p[x]; }

//(c) Nicolas T. Courtois January 2017, and based on
//Klaus Schmeh: The East German Encryption Machine T-310 and the Algorithm It Used,
//In Cryptologia, 30: 3, pp. 251 257, 2006.
void T310BlockPhiEncryptOneRound(
		int s1,int s2,int f,//extra inputs = key/IV
		int o[37],//outputs [1..36]
		int i[37]//inputs [1..36]: v[0]=s1 etc...
		)
{
	int v[37] = {0}; //internal 37 inputs: v[0]=s1 and last/proper 36
	int j = 0, k = 0;
	int t[10] = {-1, 0};//used 1..9, outputs of T

	v[0] = s1; //one extra input

	for( k = 1; k <= 36; k++ ) {
		v[k] = i[k-1+1];
	}


	T310TFunction(
			t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9],
			f,s2,
			v[P(1)],v[P(2)],v[P(3)],v[P(4)],v[P(5)],v[P(6)],v[P(7)],v[P(8)],v[P(9)],v[P(10)],
			v[P(11)],v[P(12)],v[P(13)],v[P(14)],v[P(15)],v[P(16)],v[P(17)],v[P(18)],v[P(19)],
			v[P(20)],v[P(21)],v[P(22)],v[P(23)],v[P(24)],v[P(25)],v[P(26)],v[P(27)]
			);

	for ( j = 1; j <= 9; j++ ) {
		o[4*j-3] = ( v[D(j)] + t[10-j] ) &1;
	}
	for( j = 1; j <= 9; j++) {
		o[4*j-2] = v[4*j - 3]; //starts at input v[1]
	}
	for( j = 1; j <= 9; j++ ) {
		o[4*j-1] = v[4*j - 2];
	}
	for( j = 1; j <= 9; j++ ) {
		o[4*j-0] = v[4*j - 1]; //up to output o[36]
	}
}
